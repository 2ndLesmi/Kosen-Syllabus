#!/bin/bash

PREVIOUS="0"

while : ; do
  sleep 1h
  CURRENT="$( tree -fai syllabus/ | wc -l )"
  DIFF="$(( $CURRENT - $PREVIOUS ))"
  echo "Files: $CURRENT, Delta: $DIFF"
  PREVIOUS="$CURRENT"
done
